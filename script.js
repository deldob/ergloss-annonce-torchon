// This randomizes position of list elements  
//
var litems = document.getElementsByTagName('li'); //get all list item elements
for (var i=0; i<litems.length; i++){  //loop 
	litems[i].style.position = "absolute"; //set position of each to absolute
  	var randx = (Math.random()*(180*3.7-litems[i].offsetWidth)+0).toString();
		// 180mm * 3.7 gives us size in pixels. From this, substract the element's width so it doesnt go over the page. Then multiply by random (0->1) to give it a random x position
  	var randy = (Math.random()*(70*3.7-litems[i].offsetHeight)+50*3.7).toString();
		// same here, but for height and y. To make sure it doesn't fall on the first 50mm of the page, 50mm converted to px must be added.
	console.log(litems[i].offsetWidth);
  	litems[i].style.left=randx.concat("px"); // join the string with "px"
  	litems[i].style.top=randy.concat("px");
	
	litems[i].style.transform = "rotate(" + (Math.random()*30-15) + "deg)"; //rotate item. Random multiplied by range (30) minus half of range, to allow rotation both left and right.
}


// This creates a background of 8000 UNICODE rectangle shapes

var bgchoice=["\u2591", "\u2592", "\u2593", "\u2591", "\u2592","\u2591", "\u2592", "\u25cc" ] ;
var bgstring="";
for (var i=0; i<8000; i++){
	var randomIndex = Math.floor(Math.random()*bgchoice.length);
	bgstring += bgchoice[randomIndex];
}
console.log(bgstring);
const bg = document.createElement("p");
bg.innerHTML = bgstring;
document.querySelector('.backgr').appendChild(bg);
bg.style.position = "absolute";
bg.classList.add("background");


// ADD A NON-COLLISION CLAUSE FOR FIRST FUNCTION to place elements away from eachother



//window.print();



